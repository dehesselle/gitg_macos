# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# This file contains everything related to gitg.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # multipe vars only used outside this script

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

GITG_APP_DIR=$ARTIFACT_DIR/gitg.app

GITG_APP_CON_DIR=$GITG_APP_DIR/Contents
GITG_APP_RES_DIR=$GITG_APP_CON_DIR/Resources
GITG_APP_BIN_DIR=$GITG_APP_RES_DIR/bin
GITG_APP_ETC_DIR=$GITG_APP_RES_DIR/etc
GITG_APP_EXE_DIR=$GITG_APP_CON_DIR/MacOS
GITG_APP_LIB_DIR=$GITG_APP_RES_DIR/lib

GITG_APP_PLIST=$GITG_APP_CON_DIR/Info.plist

GITG_BUILD=${GITG_BUILD:-0}

### functions ##################################################################

function gitg_get_version_from_module {
  xmllint --xpath "string(//moduleset/meson[@id='gitg']/branch/@version)" \
    "$ETC_DIR"/modulesets/gitg/gitg.modules
}

function gitg_get_version_from_meson {
  local gitg_src_tarball
  gitg_src_tarball=$(basename "$(xmllint \
      --xpath "string(//moduleset/meson[@id='gitg']/branch/@module)" \
      "$ETC_DIR"/modulesets/gitg/gitg.modules
      )"
  )

  grep version "$SRC_DIR/${gitg_src_tarball%%.*}"/meson.build |
    head -1 |
    awk -F "'" '{ print $2 }'
}

### main #######################################################################

# Nothing here.

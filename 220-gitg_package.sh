#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Create the application bundle and make everything relocatable.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

#------------------------------------------------------ source jhb configuration

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

#------------------------------------------- source common functions from bash_d

# bash_d is already available (it's part of jhb configuration)

bash_d_include error
bash_d_include lib

### variables ##################################################################

SELF_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" || exit 1; pwd)

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#----------------------------------------------------- create application bundle

( # run gtk-mac-bundler

  cd "$SELF_DIR" || exit 1
  export ARTIFACT_DIR   # is referenced in gitg.bundle

  jhb run gtk-mac-bundler resources/gitg.bundle
)

lib_change_siblings "$GITG_APP_LIB_DIR"

#------------------------------------------------------------- update Info.plist

# https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/CocoaKeys.html

# enable HiDPI
/usr/libexec/PlistBuddy -c "Add NSHighResolutionCapable bool 'true'" \
  "$GITG_APP_PLIST"

# update minimum system version according to deployment target
if [ -z "$MACOSX_DEPLOYMENT_TARGET" ]; then
  MACOSX_DEPLOYMENT_TARGET=$SYS_SDK_VER
fi
/usr/libexec/PlistBuddy \
  -c "Set LSMinimumSystemVersion $MACOSX_DEPLOYMENT_TARGET" \
  "$GITG_APP_PLIST"

# set gitg version
/usr/libexec/PlistBuddy -c "Set CFBundleShortVersionString \
  \"$(gitg_get_version_from_meson)\"" "$GITG_APP_PLIST"
/usr/libexec/PlistBuddy -c \
  "Set CFBundleVersion '$GITG_BUILD'" "$GITG_APP_PLIST"

# set copyright
/usr/libexec/PlistBuddy -c "Set NSHumanReadableCopyright 'Copyright © 2015-\
$(date +%Y) gitg developers'" "$GITG_APP_PLIST"

# set app category
/usr/libexec/PlistBuddy -c "Add LSApplicationCategoryType string \
  'public.app-category.developer-tools'" "$GITG_APP_PLIST"

# set folder access descriptions
/usr/libexec/PlistBuddy -c "Add NSDesktopFolderUsageDescription string \
  'gitg needs your permission to access the Desktop folder.'" "$GITG_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDocumentsFolderUsageDescription string \
  'gitg needs your permission to access the Documents folder.'" "$GITG_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDownloadsFolderUsageDescription string \
  'gitg needs your permission to access the Downloads folder.'" "$GITG_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSRemoveableVolumesUsageDescription string \
  'gitg needs your permission to access removable volumes.'" "$GITG_APP_PLIST"

# add supported languages
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations array" "$GITG_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string 'en'" \
  "$GITG_APP_PLIST" # because there is no en.po file (default localization)
for locale in "$SRC_DIR"/gitg-*/po/*.po; do
  /usr/libexec/PlistBuddy -c \
    "Add CFBundleLocalizations: string '$(basename -s .po $locale)'" \
    "$GITG_APP_PLIST"
done

# add some metadata to make CI identifiable
if $CI_GITLAB; then
  for var in PROJECT_NAME PROJECT_URL COMMIT_BRANCH COMMIT_SHA \
    COMMIT_SHORT_SHA JOB_ID JOB_URL JOB_NAME PIPELINE_ID PIPELINE_URL; do
    # use awk to create camel case strings (e.g. PROJECT_NAME to ProjectName)
    /usr/libexec/PlistBuddy -c "Add CI$(
      echo $var | awk -F _ '{
        for (i=1; i<=NF; i++)
        printf "%s", toupper(substr($i,1,1)) tolower(substr($i,2))
      }'
    ) string $(eval echo \$CI_$var)" "$GITG_APP_PLIST"
  done
fi

# gitg for macOS

![GitLab master branch](https://gitlab.com/dehesselle/gitg_macos/badges/master/pipeline.svg)
![Latest Release](https://img.shields.io/gitlab/v/release/dehesselle/gitg_macos?sort=semver&color=2f699b&label=Latest%20Release)

![gitg](resources/gitg_macos.png)

This is [gitg](https://wiki.gnome.org/Apps/Gitg/) for macOS. The app is standalone, relocatable and supports macOS Catalina up to macOS Ventura. (Sonoma likely works, but untested.)

## download

Downloads are available in the [Releases](https://gitlab.com/dehesselle/gitg_macos/-/releases) section.

## license

This work is licensed under [GPL-2.0-or-later](LICENSE).  
gitg is licensed under [GPL-2.0-or-later](https://gitlab.gnome.org/GNOME/gitg/-/blob/master/COPYING).

### additional credits

Built using other people's work:

- [gtk-osx](https://gitlab.gnome.org/GNOME/gtk-osx) licensed under GPL-2.0-or-later.
